# Mapa conceptual "Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)"


```plantuml 
@startmindmap 
*[#C1FFA0] Programación Declarativa 

 *[#lightblue] Orientaciones
  
  * Surgimiento
   *_ reaccion a
    * Programacion imperativa
     * Demasiados detalles
     * Asignaciones 
      *_ como intrumento de
       * Modificacion
        * Estado de memoria
     *_ basada en 
      * Modelo von Neumann
   *_ como alternativa
    * Expresiva
     *_ para 
      * Programar
       *_ liberado de
        * Especificaciones
     *_ priorizando 
      * Codigo funcional      

  * Ideas
   *_ liberarse de
    * Asignaciones
     *_ evitando tareas de
      * Gestion de memoria
   *_ acercarse a 
    * Mente del programador
     *_ enfasis en
      * Creatividad
     *_ evitando
      * BottleNeck intelectual  
   * Proposito
    *_ disminuir
     * Complejidad
      *_ mediante reduccion de
       * procesos 
    *_ simplificar
     * Programas
      *_ acortando cantidad de
       * Codigo
      *_ mas faciles de
       * Desarrollar
      *_ mas facil de 
       * Depurar  
    *_ reducir probabilidad de
     * Errores 
      *_ reduciendo
       * Control de memoria
      *_ evitando
       * Especificaciones
        *_ con gran
         * Nivel de detalle  
   * Complicaciones
    *_ nivel muy alto de
     * Abstraccion
      *_ podria causar problemas de
       * Eficiencia
    *_ utiliza una
     * Concepcion
      *_ no muy 
       * Natural
        *_ en la 
         * Mente promedio   
   * Ventajas
    *_ reduccion del tiempo de
     * Desarrollo
     * Modificacion
     * Depuracion    
   
 *[#lightblue] Tipos 
  * Programacion funcional
   *_ recurre al   
    * Lenguaje matematico
     *_ especificamente para
      * Describir funciones
   * Caracteristicas
    *_ existencia de
     * Funciones de orden superior
      *_ es la aplicacion de 
       * Funciones
        *_ sobre
         * Datos
         * Funciones  
    * Evaluacion perezosa
     *_ retrasa
      * Calculos de expresion
       *_ hasta necesidad de
        * Valor
     *_ mejorando
      * Tiempo de ejecucion   
   *_ enfoque 
    * Programas
     *_ como
      * Funciones
       *_ tienen
        * Datos de entrada
       *_ generan
        * Datos de salida
       *_ trabajan mediante
        * Reglas de simplificacion      

  * Programacion Logica
   *_ acude a logica de
    * Predicados de primer orden
     *_ son
      * Relaciones entre objetos 
       *_ sin establecer
        * Orden
         * Argumentos de entrada
         * Datos de salida
     *_ evitando recurrir al
      * Lenguaje matematico
       *_ para definir
        * Funciones
     *_ permite ser mas
      * Declarativos
       *_ al no establecer
        * Direccion
         *_ de
          * Procesamiento   
      
   *_ se basa en 
    * Modelo de demostracion logica   
     *_ utiliza
      * Expresiones
       * Axiomas
       * Reglas de inferencia 
     * Compilador
      *_ es un
       * Motor de inferencia
    *_ sin distincion entre
     * Datos de entrada
     * Datos de salida   
  @endmindmap 
  ```

# Mapa conceptual "Lenguaje de Programación Funcional (2015)"

```plantuml 
@startmindmap 
*[#C1FFA0] Lenguaje de Programación Funcional 
 *[#lightblue] Paradigma funcional
  * Paradigma
   *_ es un
    * Modelo de computacion
     
     *_ en un inicio basadados en
      * Modelo de von Neumann
     
       *_ brinda
        * Semantica
         *_ a
          * Programas 

       * Programas
        *_ almacenados en el
         * Computador
          *_ antes de ser
           * Ejecutados
        *_ es una
         * Coleccion de datos
          *_ con 
           * Serie de instrucciones   
     
       * Ejecucion
        *_ consiste en
         * Serie de intrucciones
          *_ de ejecucion 
           * Secuencial 

     
       * Secuencia de ejecucion
        *_ de acuero al 
         * Estado de computo
          *_ es una
           * Zona de memoria

       * Intruccion de asignacion
        *_ establece a una
         * Variable
          *_ un determinado
           * Valor fijo

       *_ da lugar a 
        * Lenguajes imperativos
         * Instrucciones
          *_ como 
           * Ordenes
         *_ nace
          * Programacion orienta a objetos

     *_ basado en 
      * Logica simbolica
       *_ da lugar a 
        * Paradigma logico
         * Programa
          * Conjunto
           *_ de 
            * Sentencias
          *_ define lo que es
           * Verdad
            *_ y 
             * Conocido
          *_ resuelve 
           * Problemas
            *_ mediante
             * Inferencia logica
          *_ sin obligatoria
           * Secuencia de pasos

     * Lambda Calculus
      *_ desarrollado por
       * Church-Kleene
        *_ en decada de
         * 1930
      *_ inicialmente pensado
       * Estudio de funciones 
        *_ generó
         * Sistema computable
          *_ equivalente al de
           * von Neumann
        * Aplicacion de funciones
        * Recursividad
      *_ continuo
       * Haskell Curry
        *_ desarrolla
         * Logica combinatoria
          *_ utiliza
           * Peter Landin
            *_ establece bases de
             * Programacion funcional  


  * Programa
   *_ es practicamente una
    * Funcion
     *_ en concepcion puramente
      * Matematica
       * Ejemplo
        * X + I
     * Consecuencias 
      *_ nueva concepcion de
       * Asignacion
        *_ cambia concepto de
         * Variable
          *_ como
           * Zona de memoria
        *_ desaparece
         * Estado de computo
          *_ de
           * Modelo de von Neumann

      * Estado de computo
      * Recursion
       *_ es una 
        * Referencia a si mismo
       *_ brinda mayor
        * Eficiencia 

      * Ventajas
       *_ como  
        * Herramienta de prototipado
         *_ en distintos
          * Campos de aplicacion 
       *_ en un   
        * Entorno multiprocesable
         *_ permite
          * Paralelizar ejecuciones   
      

     *_ utiliza
      * Variables
       *_ solo como
        * Parametros de entrada

      
      
     * Transparencia referecial
      *_ es
       * Resultados independientes
        *_ de 
         * Orden de calculos
          *_ al no contar con
           * Estado de computo
      *_ solo depende de
       * Parametros de entrada
        *_ ante una misma
         * Entrada
          *_ una misma
           * Salida
     * Currificacion
      *_ es una 
       * Funcion
        *_ con diferentes
         * Parametros de entrada
          *_ que duvuelve varias
           * Salidas
        *_ lleva a una
         * Aplicacion parcial    
     * Evaluacion perezosa
      *_ es una 
       * Evaluacion
        *_ desde
         * Fuera hacia adentro 
      * Memoización
       *_ es
        * Almacenar valor
         *_ de
          * Operaciones evaluadas         

   


 
 
  @endmindmap 
  ```
